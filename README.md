# Gold Box Editor

This project provides tools for editing save files from the classic SSI Gold Box games.

## Requirements

The editor tool is built in Java using the 1.8 edition. To use the editor you must have a java 1.8 (or 
later) JVM installed. To compile the editor from code you need JDK1.8 and Maven installed.

## Usage

The editor runs as a command-line tool, to start the editor run `java -jar goldboxeditor-<version>-jar-with-dependencies.jar`.
Once started the editor presents a simple prompt:

```
Gold Box Editor
Action >
```

You can list all commands by entering `?list`

Currently, the commands are

* cd
* pwd
* ls
* load
* save
* map
* fields
* print
* set
* byteset
* show

Each command must be entered into the command-line, with the necessary parameters as appropriate, 
returning the output to the console

### file operations

#### cd (path)

Allows you to move the working directory to a specific path, this can either be an absolute path or a path relative to
the current directory

#### pwd

Show the current working directory

#### ls

Lists all valid save files present in the working directory. No other files are listed

#### load (character, save)

Loads a character from a save file. The command requires two parameters: character, save  
The character value must be a number  
The save value is a letter matching the game save slot

#### save

Saves the loaded character to a save file. There is no option to change the character or save slot, the command
will simply write back to where it was loaded from

### general operations

#### map

Shows the hexadecimal representation of the loaded character

### character commands

#### fields (name)

Lists the name of all valid fields for the loaded character. If the name is an empty string `""` or `all` the
output is a list of all fields. Any other value is used to find field names that start with the given name

#### set (field, value)

Modifies the named field to use the given value, or as close as possible. The set command adheres to limits to
the value according to the field to avoid corrupting the save data where possible

#### byteset (offset, value)

Modifies a single byte within the save file, modifying it to the given value. **This is potentially dangerous**
and can render the file unusable. Please make sure you know what you are doing before you change values. The
offset can exceed the file limits, the value can be any value from 0 to 255

#### show (field)

Displays the value of the given field name. If field is a question mark the list of all valid field names is
returned (this is the same behaviour as `fields all`)

## Building the editor

The editor is coded in Java using Maven for dependency and build management.    
To build the editor simply use `mvn clean package`

The output of the build is placed in the target folder in two forms, one with all required dependencies and one
without. It is recommended to use the fat version for portability

### Configuration

The game formats are built from the core configuration file following a basic schema (WIP):

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "Formats": {
      "type": "object",
      "properties": {
        "FormatName": {
          "type": "object",
          "properties": {
            "Fields": {
              "type": "array",
              "items": {}
            }
          },
          "required": [
            "Fields"
          ]
        }
      },
      "required": [
        "FormatName"
      ]
    },
    "Lists": {
      "type": "object",
      "properties": {
        "ListName": {
          "type": "array",
          "items": {}
        }
      },
      "required": [
        "ListName"
      ]
    }
  },
  "required": [
    "Formats",
    "Lists"
  ]
}
```

The format objects can be supplemented with fields to define specific lists needed to complete the format definition.

The main field is the `Fields` array that is populated with field definitions