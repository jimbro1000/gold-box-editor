# Changelog #

## V1.0.1-3 2022-04-27 ##
* Handle constrained field values
* Provide translation between offset and fields
* Load format configuration from resources

## V1.0.0-2 2022-04-21 ##

First full release of editor

* Populate game formats for Faerun series
* Add status and enabled field to Faerun and Krynn formats
* Populate Unlimited Adventures formats
* Populate Buck Rogers format
* Populate Savage Frontier formats

## V1.0.0-1 2022-04-20 ##
* Update capability for saved character files
* Interrogate game format to expose valid fields
* Constrain update values to avoid corruptions

## V1.0.0-0 2020-03-01 ##
* Basic interrogation commands for saved character files
* Working path navigation
* Load character
* 'Print' character
* Auto-detection of different game formats