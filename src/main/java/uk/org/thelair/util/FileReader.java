package uk.org.thelair.util;

import java.io.File;
import java.io.IOException;

public interface FileReader {
  byte[] read(File file) throws IOException;
}
