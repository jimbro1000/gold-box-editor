package uk.org.thelair.util;

import java.io.File;
import java.io.IOException;

public interface FileWriter {
  void write(File file, byte[] buffer) throws IOException;
}
