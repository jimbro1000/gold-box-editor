package uk.org.thelair.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
public class SafeFileUtility {
  /**
   * Fetch a file reference for a single named file.
   *
   * @param path     path to search for file
   * @param filename name of file to retrieve
   * @return file reference instance
   */
  public File getFile(final String path, final String filename) {
    String[] pathParts = new String[]{filename};
    java.nio.file.Path nioPath = java.nio.file.Paths.get(path, pathParts);
    File file = new File(
        nioPath.toString()
    );
    if (file.exists()) {
      if (file.isDirectory()) {
        file = null;
      }
    } else {
      file = null;
    }
    return file;
  }

  /**
   * Provides a filter list of files in the given path.
   *
   * @param path   filepath to search
   * @param filter Filename filter to apply
   * @return lists of files in path matching filter
   */
  public List<File> getFilesInDirectory(
      final String path,
      final FilenameFilter filter
  ) {
    List<File> files = new ArrayList<>();
    File folder = new File(path);
    if (folder.isDirectory()) {
      File[] fileList = folder.listFiles(filter);
      if (fileList != null) {
        files.addAll(Arrays.asList(fileList));
      }
    }
    return files;
  }
}
