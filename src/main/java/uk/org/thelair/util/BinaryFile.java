package uk.org.thelair.util;

import uk.org.thelair.goldbox.formats.*;

import java.io.File;
import java.io.IOException;

public class BinaryFile {
  static final String BLOCK_HEADER =
      "      00....03 04....0F 08....0B 0C....0F | 0..3 4..7 8..B C..F";
  static final String UNPRINTABLE_CHAR = ".";
  byte[] binaryBuffer;
  private File sourceFile;
  private FileWriter writer;
  private int bytesRead;
  private FileReader reader;

  public static final int BYTE_MASK = 255;
  public static final int BYTE_WIDTH = 8;

  /**
   * Constructor.
   *
   * @param fileReader injected file reader
   * @param fileWriter injected file writer
   */
  public BinaryFile(FileReader fileReader, FileWriter fileWriter) {
    reader = fileReader;
    writer = fileWriter;
  }

  private static String intToHexString(int value) {
    String hexResult = "";
    if (value >= -128 && value < 128) {
      hexResult = Integer.toHexString(value & 0xFF);
    } else {
      hexResult = Integer.toHexString(value);
    }

    if (hexResult.length() % 2 != 0) {
      hexResult = "0" + hexResult;
    }
    return hexResult;
  }

  private static String safeAsciiToString(int value) {
    String result = UNPRINTABLE_CHAR;
    if (value > 31) {
      result = Character.toString((char) value);
    }
    return result;
  }

  /**
   * Read given file to buffer.
   *
   * @param sourceFile specified game file
   * @throws IOException when the file is unreadable
   */
  public void load(File sourceFile) throws IOException {
    this.sourceFile = sourceFile;
    binaryBuffer = reader.read(sourceFile);
    bytesRead = binaryBuffer.length;
  }

  /**
   * Write buffer to original file.
   *
   * @throws IOException when the file is unwriteable
   */
  public void write() throws IOException {
    writer.write(sourceFile, binaryBuffer);
  }

  /**
   * Fetch byte from buffer at the given offset.
   *
   * @param field FieldDefinition(offset, length, type)
   * @return byte at the given offset
   * @throws IndexOutOfBoundsException when the offset is out of range
   */
  public byte peek(FieldDefinition field) {
    return peek(field.byteOffset);
  }

  /**
   * Fetch byte from buffer at the given offset.
   *
   * @param offset byte address to fetch from
   * @return byte at the given offset
   * @throws IndexOutOfBoundsException when the offset is out of range
   */
  public byte peek(int offset) {
    if (offset < 0 || offset >= bytesRead) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    return binaryBuffer[offset];
  }

  /**
   * Modify byte in buffer at the given offset.
   *
   * @param value  byte value to update
   * @param offset addressing offset
   * @throws IndexOutOfBoundsException when the offset is out of range
   */
  public void poke(byte value, int offset) {
    if (offset < 0 || offset >= bytesRead) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    binaryBuffer[offset] = value;
  }

  public void pokeWord(final int value, final int offset) {
    if (offset < 0 || offset >= (bytesRead - 1)) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    final byte low = (byte) (value & BYTE_MASK);
    final byte high = (byte) (value >> BYTE_WIDTH);
    poke(low, offset);
    poke(high, offset + 1);
  }

  public void pokeDoubleWord(final long value, final int offset) {
    if (offset < 0 || offset >= (bytesRead - 3)) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    long targetLong = value;
    final byte b0 = (byte) (targetLong & BYTE_MASK);
    targetLong = targetLong >> BYTE_WIDTH;
    final byte b1 = (byte) (targetLong & BYTE_MASK);
    targetLong = targetLong >> BYTE_WIDTH;
    final byte b2 = (byte) (targetLong & BYTE_MASK);
    final byte b3 = (byte) (targetLong >> BYTE_WIDTH);
    poke(b0, offset);
    poke(b1, offset + 1);
    poke(b2, offset + 2);
    poke(b3, offset + 3);
  }

  /**
   * The number of bytes in the buffer.
   *
   * @return buffer size
   */
  public int size() {
    return bytesRead;
  }

  /**
   * Format buffer as a hex map.
   *
   * @return formatted string
   */
  public String toHexString() {
    String result = BLOCK_HEADER + "\n";
    int pointer = 0;
    final int padLength = 16 - binaryBuffer.length % 16;
    while (pointer < binaryBuffer.length) {
      if (pointer < 256) {
        result = result.concat("00");
      }
      result = result.concat(intToHexString(pointer));
      result = result.concat("  ");
      String charResult = " | ";
      for (int index = 0; index < 16; ++index) {
        if (pointer + index < binaryBuffer.length) {
          if (index % 4 == 0 && index != 0) {
            result = result.concat(" ");
            charResult = charResult.concat(" ");
          }
          result = result.concat(intToHexString(binaryBuffer[pointer + index]));
          charResult = charResult.concat(safeAsciiToString(binaryBuffer[pointer + index]));
        }
      }
      pointer += 16;
      if (pointer > binaryBuffer.length) {
        for (int index = 0; index < padLength; ++index) {
          if (index % 4 == 0 && index != 0) {
            result = result.concat(" ");
            charResult = charResult.concat(" ");
          }
          result = result.concat("  ");
          charResult = charResult.concat(" ");
        }
      }
      result = result.concat(charResult).concat("\n");
    }
    return result;
  }

  /**
   * Fetch array of bytes from the buffer.
   *
   * @param offset addressing offset to start copy
   * @param length number of bytes to copy
   * @return detached array of bytes
   */
  public byte[] copyBufferToArray(int offset, int length) {
    if (offset < 0 || offset >= bytesRead || offset + length >= bytesRead) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    byte[] targetArray = new byte[length];
    System.arraycopy(binaryBuffer, offset, targetArray, 0, length);
    return targetArray;
  }

  /**
   * Copy array of bytes to the buffer.
   *
   * @param offset      addressing offset to start copy
   * @param sourceArray array of bytes to copy
   */
  public void copyArrayToBuffer(int offset, byte[] sourceArray) {
    if (offset < 0 || offset >= bytesRead || offset + sourceArray.length >= bytesRead) {
      throw new IndexOutOfBoundsException("Address offset out of range");
    }
    System.arraycopy(sourceArray, 0, binaryBuffer, offset, sourceArray.length);
  }

  /**
   * Fetch the name of game represented.
   *
   * @return game format name
   */
  public String getName() {
    return sourceFile.getName();
  }

  /**
   * Fetch null-terminated string from buffer as a byte array.
   *
   * @param field FieldDefinition (offset, length, type)
   * @return byte array of string (not null terminated)
   */
  public byte[] extractString(FieldDefinition field) {
    int index = field.byteOffset;
    while (index < binaryBuffer.length && peek(index) != 0) {
      ++index;
    }
    return copyBufferToArray(field.byteOffset, index - field.byteOffset);
  }
}
