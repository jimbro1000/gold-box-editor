package uk.org.thelair.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class FileHandler {
  private FilenameFilter savFilter;
  private SafeFileUtility fileUtility;

  public FileHandler(final SafeFileUtility safeFileUtility, final FilenameFilter fileFilter) {
    fileUtility = safeFileUtility;
    savFilter = fileFilter;
  }

  /**
   * Generate File instance for the given same game file.
   *
   * @param path   path to search for file
   * @param fileId file id (1-6)
   * @param game   save game (A-G)
   * @return File instance
   */
  public File getFileWithId(
      final String path,
      final int fileId,
      final String game) {
    String filename = String.format("CHRDAT" + game + "%d.SAV", fileId);
    return fileUtility.getFile(path, filename);
  }

  /**
   * Fetch a list of game files in the given directory path.
   *
   * @param path search path
   * @return list of matched file names
   */
  public List<String> getFilesInDirectory(final String path) {
    List<String> files = new ArrayList<>();
    for (File file : fileUtility.getFilesInDirectory(path, savFilter)) {
      files.add(file.getName());
    }
    return files;
  }
}
