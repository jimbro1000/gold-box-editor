/**
 * Domain classes for system utilities
 * <p>
 * These classes contain the file and file system interaction
 * </p>
 *
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
package uk.org.thelair.util;