package uk.org.thelair.util;

import org.json.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;

public class FormatLoader {
  private final JSONObject config;
  private final JSONObject formats;
  private final JSONObject lists;

  public FormatLoader() {
    String formatJson = "{}";
    try {
      formatJson = readFileFromResources("FormatConfiguration.json");
    } catch (Exception ex) {
      System.out.println("Failed to load configuration");
    }
    config = new JSONObject(formatJson);
    formats = config.getJSONObject("Formats");
    lists = config.getJSONObject("Lists");
  }

  private static String readFileFromResources(String filename) throws URISyntaxException, IOException {
    final URL resource = FormatLoader.class.getClassLoader().getResource(filename);
    return new String(Files.readAllBytes(Paths.get(resource.toURI())));
  }

  public JSONObject getFormat(String formatName) {
    if (formats.has(formatName)) {
      return formats.getJSONObject(formatName);
    }
    return null;
  }

  private List<String> listConverter(List<Object> original) {
    List<String> result = new ArrayList<>();
    for (Object o : original) {
      result.add((String) o);
    }
    return result;
  }

  public List<String> getList(String listName) {
    if (lists.has(listName)) {
      JSONArray jsonArray = lists.getJSONArray(listName);
      return listConverter(jsonArray.toList());
    }
    return new ArrayList<String>();
  }
}
