package uk.org.thelair.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class ByteToFileWriter implements FileWriter {
  @Override
  public void write(File file, byte[] buffer) throws IOException {
    Files.write(file.toPath(), buffer);
  }
}
