package uk.org.thelair.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileToByteReader implements FileReader {
  public byte[] read(File file) throws IOException {
    return Files.readAllBytes(file.toPath());
  }
}