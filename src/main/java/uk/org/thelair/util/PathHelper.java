package uk.org.thelair.util;

import java.io.File;
import java.io.IOException;

public class PathHelper {
  public static final String WINDOWS_SEPARATOR = "\\\\";
  public static final String NIX_SEPARATOR = "/";
  private static final String WINDOWS_DRIVE_MATCH = ":\\";

  public String canonicalPath(String path) throws IOException {
    File filePath = new File(path);
    return filePath.getCanonicalPath();
  }

  public boolean isWindowsOs() {
    final String home = System.getProperty("user.home");
    return WINDOWS_DRIVE_MATCH.equals(home.substring(1, 3));
  }

  /**
   * Resolves change to the source path.
   *
   * @param path      source path
   * @param pathDelta path change
   * @return resolved path
   * @throws IOException when the path does not or cannot exist
   */
  public String resolveChangeToPath(
      String path, String pathDelta
  ) throws IOException {
    boolean winOsFlag = false;
    String separator = NIX_SEPARATOR;
    String resultPath;

    final String home = System.getProperty("user.home");

    winOsFlag = isWindowsOs();
    if (winOsFlag) {
      separator = WINDOWS_SEPARATOR;
    }
    String sourcePath = canonicalPath(path);
    //catch home  ~
    if (pathDelta.startsWith("~")) {
      sourcePath = home;
      if (pathDelta.length() > 1) {
        pathDelta = pathDelta.substring(2);
      } else {
        pathDelta = "";
      }
    }
    //catch root for nix /
    if (pathDelta.startsWith("/")) {
      sourcePath = pathDelta;
      pathDelta = "";
    }
    //catch drive for windows X:\
    if (!"".equals(pathDelta) && ":\\".equals(pathDelta.substring(1, 3))) {
      if (winOsFlag) {
        sourcePath = pathDelta;
        pathDelta = "";
      } else {
        throw new IOException("Illegal file path");
      }
    }

    if (!"".equals(pathDelta)) {
      //split file path parts
      String[] pathParts = pathDelta.split(separator);

      //rebuild using nio.file.paths.get
      java.nio.file.Path nioPath = java.nio.file.Paths.get(sourcePath, pathParts);
      if (!java.nio.file.Files.exists(nioPath)) {
        throw new IOException("File path does not exist");
      } else {
        resultPath = nioPath.toString();
      }
    } else {
      resultPath = sourcePath;
    }
    return resultPath;
  }
}
