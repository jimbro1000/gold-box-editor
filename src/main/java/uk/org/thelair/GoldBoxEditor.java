package uk.org.thelair;

import uk.org.thelair.menu.EditorMenu;
import uk.org.thelair.util.FileHandler;
import uk.org.thelair.util.PathHelper;
import uk.org.thelair.util.SafeFileUtility;

import java.io.FilenameFilter;
import java.io.IOException;

/**
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
public final class GoldBoxEditor {
  /**
   * Public constructor and initiator of main process.
   */
  private GoldBoxEditor() {
    SafeFileUtility fileUtility = new SafeFileUtility();
    FilenameFilter goldBoxFilter = (dir, name) -> {
      String lowercaseName = name.toLowerCase();
      return lowercaseName.endsWith(".sav");
    };
    FileHandler fileHandler = new FileHandler(fileUtility, goldBoxFilter);
    PathHelper pathHelper = new PathHelper();
    try {
      new EditorMenu(fileHandler, pathHelper);
    } catch (IOException ex) {
      System.out.println("Fatal error opening application");
    }
  }

  /**
   * static application entry point.
   *
   * @param args command line arguments (ignored)
   */
  public static void main(final String[] args) {
    new GoldBoxEditor();
  }
}
