package uk.org.thelair.goldbox;

import uk.org.thelair.util.BinaryFile;
import uk.org.thelair.util.FileReader;
import uk.org.thelair.util.FileWriter;

import java.io.File;
import java.io.IOException;

/**
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
public class GoldBoxCharacterBuilder {
  /**
   * Injected file reader.
   */
  private FileReader fileReaderInstance;
  /**
   * Injected file writer.
   */
  private FileWriter fileWriterInstance;

  /**
   * Constructor for character builder class.
   *
   * @param fileReader instance of file reader
   * @param fileWriter instance of file writer
   */
  public GoldBoxCharacterBuilder(
      final FileReader fileReader,
      final FileWriter fileWriter
  ) {
    fileReaderInstance = fileReader;
    fileWriterInstance = fileWriter;
  }

  /**
   * Factory method for building new character instances.
   *
   * @param file reference to saved character file
   * @return valid character instance
   * @throws IOException when file is unreadable or doesnt comply
   */
  public GoldBoxCharacter buildCharacter(
      final File file
  ) throws IOException {
    return new GoldBoxCharacter(
        file,
        new BinaryFile(fileReaderInstance, fileWriterInstance)
    );
  }
}
