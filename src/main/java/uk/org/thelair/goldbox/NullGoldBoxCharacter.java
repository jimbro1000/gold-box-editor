package uk.org.thelair.goldbox;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class NullGoldBoxCharacter extends GoldBoxCharacter {
    private static final String RETURN_MESSAGE = "No character loaded";

    public NullGoldBoxCharacter() {
        super();
    }

    public void load() {}
    public void save() throws IOException {}
    public String formattedToString() {
        return RETURN_MESSAGE;
    }
    public String setField(final String fieldName, final String value) {
        return RETURN_MESSAGE;
    }
    public String getName() {
        return RETURN_MESSAGE;
    }
    public long getField(final String fieldName) {
        return 0;
    }
    public Set<String> getFields() {
        return new HashSet<>();
    }
    public String getCharacterClasses() {
        return RETURN_MESSAGE;
    }
    public String getCharacterLevels() {
        return RETURN_MESSAGE;
    }
    private String getCharacterStats() {
        return RETURN_MESSAGE;
    }

    private String getMoney() {
        return RETURN_MESSAGE;
    }

    public String getCharacterSheet() {
        return RETURN_MESSAGE;
    }

    public String getFormatName() {
        return RETURN_MESSAGE;
    }

    public String getOffset(int offset) {
        return RETURN_MESSAGE;
    }
}
