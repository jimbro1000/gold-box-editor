/**
 * Domain classes used to produce gold box specific character details
 * <p>
 * These classes contain the general character and character factory
 * </p>
 *
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
package uk.org.thelair.goldbox;