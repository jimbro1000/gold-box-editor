package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.FormatLoader;

import java.util.*;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class FaerunTwo extends Faerun {
  private static FaerunTwo instance;

  private static final Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private FaerunTwo() {
    //null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Curse of the Azure Bonds game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new FaerunTwo();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Curse of the Azure Bonds";
  }

  @Override
  public Long getFileLength() {
    return 422L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("CurseOfTheAzureBonds");
    loadFieldsFromConfig(loader, format);
  }
}
