package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.FormatLoader;

import java.util.*;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class FaerunOne extends Faerun {
  private static FaerunOne instance;

  private static final Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private FaerunOne() {
    //hidden constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Pools of Radiance game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new FaerunOne();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Pool Of Radiance";
  }

  @Override
  public Long getFileLength() {
    return 285L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("PoolOfRadiance");
    loadFieldsFromConfig(loader, format);
  }
}
