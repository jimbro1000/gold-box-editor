package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.FormatLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.0-2
 * @since 0.0.0
 */
public class KrynnThree extends Krynn {
  private static KrynnThree instance;

  private static List<String> CURRENCY = new ArrayList<String>() {
    {
      add("Steel");
      add("Gems");
      add("Jewelry");
    }
  };
  private static List<String> CLASSES = new ArrayList<String>() {
    {
      add("Cleric");
      add("Fighter");
      add("Paladin");
      add("Ranger");
      add("Mage");
      add("Thief");
      add("Knight");
      add("Monk");
    }
  };

  private static List<String> ALL_CLASSES = new ArrayList<String>() {
    {
      add("cleric");
      add("knight");
      add("fighter");
      add("paladin");
      add("ranger");
      add("mage");
      add("thief");
      add("monk");
      add("cleric/fighter");
      add("cleric/fighter/mage");
      add("cleric/ranger");
      add("cleric/mage");
      add("cleric/thief");
      add("fighter/mage");
      add("fighter/thief");
      add("fighter/mage/thief");
      add("mage/thief");
      add("illegal type");
    }
  };

  private static List<String> STAT_KEYS = new ArrayList<String>() {
    {
      add("Strength");
      add("Intelligence");
      add("Wisdom");
      add("Dexterity");
      add("Constitution");
      add("Charisma");
    }
  };
  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
    }
  };
  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<String, FieldDefinition>();

  private KrynnThree() {
    // null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return The Dark Queen of Krynn game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new KrynnThree();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "The Dark Queen Of Krynn";
  }

  @Override
  public Long getFileLength() {
    return 413L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("DarkQueenOfKrynn");
    loadFieldsFromConfig(loader, format);
    CURRENCY = loader.getList((String) format.get("Money"));
  }
}
