package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.FormatLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.0-2
 * @since 0.0.0
 */
public class KrynnTwo extends Krynn {
  private static KrynnTwo instance;

  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<String, FieldDefinition>();

  private static List<String> CURRENCY;

  private KrynnTwo() {
    // null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Death Knights of Krynn game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new KrynnTwo();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Death Knights Of Krynn";
  }

  @Override
  public Long getFileLength() {
    return 215L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("DeathKnightsOfKrynn");
    loadFieldsFromConfig(loader, format);
    CURRENCY = loader.getList((String) format.get("Money"));
  }
}
