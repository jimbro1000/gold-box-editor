package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Krynn implements GameFormat {
  protected static List<String> ALL_CLASSES;
  protected static List<String> RACES;
  protected static List<String> GENDERS;
  protected static List<String> ALIGNMENTS;
  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
    }
  };
  protected static List<String> STATUSES;
  protected static List<String> GODS;
  protected static List<String> ROBES;
  protected static List<String> KNIGHTS;
  private static List<String> CURRENCY;
  private static List<String> CLASSES;
  private static List<String> STAT_KEYS;

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public String setNameLengthFieldName() {
    return "NameLength";
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    JSONObject format = loader.getFormat("Krynn");
    ALIGNMENTS = loader.getList((String) format.get("Alignment"));
    ALL_CLASSES = loader.getList((String) format.get("All_Classes"));
    CLASSES = loader.getList((String) format.get("Classes"));
    CURRENCY = loader.getList((String) format.get("Money"));
    GENDERS = loader.getList((String) format.get("Gender"));
    GODS = loader.getList((String) format.get("Gods"));
    KNIGHTS = loader.getList((String) format.get("Knights"));
    RACES = loader.getList((String) format.get("Races"));
    ROBES = loader.getList((String) format.get("Robes"));
    STAT_KEYS = loader.getList((String) format.get("Stats"));
    STATUSES = loader.getList((String) format.get("Status"));
  }
}
