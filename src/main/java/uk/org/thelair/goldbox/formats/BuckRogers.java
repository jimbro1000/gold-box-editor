package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class BuckRogers implements GameFormat {
  private static BuckRogers instance;

  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<String, FieldDefinition>() {
    {
      put("NameLength", new FieldDefinition(0, 1, FieldDefinition.NUMBER_TYPE, 1, 15));
      put("Name", new FieldDefinition(1, 15, FieldDefinition.STRING_TYPE));
      put("StrCurrent", new FieldDefinition(16, 1, FieldDefinition.NUMBER_TYPE));
      put("DexCurrent", new FieldDefinition(17, 1, FieldDefinition.NUMBER_TYPE));
      put("ConCurrent", new FieldDefinition(18, 1, FieldDefinition.NUMBER_TYPE));
      put("IntCurrent", new FieldDefinition(19, 1, FieldDefinition.NUMBER_TYPE));
      put("WisCurrent", new FieldDefinition(20, 1, FieldDefinition.NUMBER_TYPE));
      put("ChaCurrent", new FieldDefinition(21, 1, FieldDefinition.NUMBER_TYPE));
      put("TchCurrent", new FieldDefinition(22, 1, FieldDefinition.NUMBER_TYPE));
      put("StrOriginal", new FieldDefinition(23, 1, FieldDefinition.NUMBER_TYPE));
      put("DexOriginal", new FieldDefinition(24, 1, FieldDefinition.NUMBER_TYPE));
      put("ConOriginal", new FieldDefinition(25, 1, FieldDefinition.NUMBER_TYPE));
      put("IntOriginal", new FieldDefinition(26, 1, FieldDefinition.NUMBER_TYPE));
      put("WisOriginal", new FieldDefinition(27, 1, FieldDefinition.NUMBER_TYPE));
      put("ChaOriginal", new FieldDefinition(28, 1, FieldDefinition.NUMBER_TYPE));
      put("TchOriginal", new FieldDefinition(29, 1, FieldDefinition.NUMBER_TYPE));
      put("Saves", new FieldDefinition(30, 8, FieldDefinition.ARRAY_TYPE));
      put("Gender", new FieldDefinition(38, 1, FieldDefinition.CONSTRAINED_TYPE, GENDERS));
      put("Race", new FieldDefinition(39, 1, FieldDefinition.CONSTRAINED_TYPE, RACES));
      put("Class", new FieldDefinition(40, 1, FieldDefinition.CONSTRAINED_TYPE, CLASSES));
      put("LevelHighest", new FieldDefinition(41, 1, FieldDefinition.NUMBER_TYPE));
      put("Coins", new FieldDefinition(43, 2, FieldDefinition.NUMBER_TYPE));
      put("Experience", new FieldDefinition(47, 4, FieldDefinition.NUMBER_TYPE));
      put("Thac0Base", new FieldDefinition(51, 1, FieldDefinition.NUMBER_TYPE));
      put("MovementBase", new FieldDefinition(54, 1, FieldDefinition.NUMBER_TYPE));
      put("AcBase", new FieldDefinition(55, 1, FieldDefinition.NUMBER_TYPE));
      put("Age", new FieldDefinition(56, 1, FieldDefinition.NUMBER_TYPE));
      put("HpMax", new FieldDefinition(69, 1, FieldDefinition.NUMBER_TYPE));
      put("Status", new FieldDefinition(212, 1, FieldDefinition.CONSTRAINED_TYPE, STATUS));
      put("Enabled", new FieldDefinition(213, 1, FieldDefinition.NUMBER_TYPE));
      put("Thac0Current", new FieldDefinition(216, 1, FieldDefinition.NUMBER_TYPE));
      put("AcCurrent", new FieldDefinition(217, 1, FieldDefinition.NUMBER_TYPE));
      put("HpCurrent", new FieldDefinition(227, 1, FieldDefinition.NUMBER_TYPE));
      put("MovementCurrent", new FieldDefinition(229, 1, FieldDefinition.NUMBER_TYPE));
    }
  };

  private static List<String> CURRENCY;

  private static List<String> CLASSES;

  private static List<String> STAT_KEYS = new ArrayList<String>();

  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
      put("Technology", "Tch");
    }
  };

  private static List<String> RACES;

  private static List<String> GENDERS;

  private static List<String> STATUS;

  private BuckRogers() {
    // null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Buck Rogers game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new BuckRogers();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Countdown to Doomsday / Matrix Cubed";
  }

  @Override
  public Long getFileLength() {
    return 258L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public String setNameLengthFieldName() {
    return "NameLength";
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    JSONObject format = loader.getFormat("BuckRogers");
    CLASSES = loader.getList((String) format.get("Classes"));
    GENDERS = loader.getList((String) format.get("Gender"));
    CURRENCY = loader.getList((String) format.get("Money"));
    RACES = loader.getList((String) format.get("Races"));
    STAT_KEYS = loader.getList((String) format.get("Stats"));
    STATUS = loader.getList((String) format.get("Status"));
  }
}
