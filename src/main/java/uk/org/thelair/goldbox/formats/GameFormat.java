package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public interface GameFormat {
  /**
   * Fetch game format name.
   *
   * @return format name string
   */
  String getFormatName();

  /**
   * Fetch length of file format.
   *
   * @return number of bytes in file format
   */
  Long getFileLength();

  /**
   * Fetch unordered list of field keys.
   *
   * @return set of field keys
   */
  Set<String> getFieldKeys();

  /**
   * Fetch a map representation of all fields in the format.
   *
   * @return map of all fields
   */
  Map<String, FieldDefinition> getFields();

  /**
   * Fetch ordered list of currency field keys.
   *
   * @return list of currency keys
   */
  List<String> getCurrencyKeys();

  /**
   * Fetch ordered list of class field names.
   *
   * @return list of class name keys
   */
  List<String> getClassKeys();

  /**
   * Fetch ordered list of stat field keys.
   *
   * @return list of stat field keys
   */
  List<String> getStatKeys();

  /**
   * Fetch a map representation of the stat specific fields in the format.
   *
   * @return map of stat fields
   */
  Map<String, String> getStatFields();

  /**
   * Match an offset to a known field name if possible
   *
   * @param offset byte offset in file format
   * @return field name or empty string
   */
  default String offsetToField(int offset) {
    Map<String, FieldDefinition> fields = getFields();
    String fieldName = "";
    for (String key : fields.keySet()) {
      FieldDefinition field = fields.get(key);
      if (field.inRange(offset)) {
        fieldName = key;
        break;
      }
    }
    return fieldName;
  }

  /**
   * Identifies field name for setting length of character name
   *
   * @return name of field to set or empty if not required
   */
  String setNameLengthFieldName();

  void loadFromConfig(FormatLoader loader);

  /**
   * Load field definitions from configuration file
   * @param loader format loader holding top level configuration json object
   * @param format json object holding format specific details
   */
  default void loadFieldsFromConfig(FormatLoader loader, JSONObject format) {
    Map<String, FieldDefinition> fields = getFields();
    JSONArray fieldSource = format.getJSONArray("Fields");
    for (Object o : fieldSource) {
      JSONObject fieldObject = (JSONObject) o;
      String fieldName = fieldObject.getString("FieldName");
      int fieldOffset = fieldObject.getInt("Offset");
      int fieldLength = fieldObject.getInt("Length");
      String fieldType = fieldObject.getString("Type");
      List<String> fieldList = null;
      if (fieldType.equals("Constrained")) {
        fieldList = loader.getList(fieldObject.getString("List"));
      }
      Integer minValue = null;
      Integer maxValue = null;
      if (fieldType.equals("Number")) {
        if (fieldObject.has("Min")) {
          minValue = fieldObject.getInt("Min");
        }
        if (fieldObject.has("Max")) {
          maxValue = fieldObject.getInt("Max");
        }
      }
      FieldDefinition fd = FieldDefinitionFactory.getFieldDefinition(fieldOffset, fieldLength, fieldType, fieldList, minValue, maxValue);
      fields.put(fieldName, fd);
    }
  }
}
