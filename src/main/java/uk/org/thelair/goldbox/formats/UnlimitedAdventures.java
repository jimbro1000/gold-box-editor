package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.*;

import java.util.*;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class UnlimitedAdventures implements GameFormat {
  private static UnlimitedAdventures instance;

  private UnlimitedAdventures() {
    //null constructor
  }

  private static List<String> ALIGNMENTS;

  private static List<String> RACES;

  private static List<String> GENDERS;

  private static List<String> CLASSES;

  private static List<String> STATUS;

  private static List<String> CURRENCY;
  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private static List<String> STAT_KEYS;

  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
    }
  };

  /**
   * Get instance for singleton.
   *
   * @return Unlimited Adventures game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new UnlimitedAdventures();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Unlimited Adventures";
  }

  @Override
  public Long getFileLength() {
    return 397L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public String setNameLengthFieldName() {
    return "";
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    JSONObject format = loader.getFormat("UnlimitedAdventures");
    loadFieldsFromConfig(loader, format);
    ALIGNMENTS = loader.getList(format.getString("Alignment"));
    CLASSES = loader.getList(format.getString("Classes"));
    GENDERS = loader.getList(format.getString("Gender"));
    CURRENCY = loader.getList(format.getString("Money"));
    RACES = loader.getList(format.getString("Races"));
    STAT_KEYS = loader.getList(format.getString("Stats"));
    STATUS = loader.getList(format.getString("Status"));
  }
}
