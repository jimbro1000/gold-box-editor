package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.*;

public abstract class Faerun implements GameFormat {
  protected static List<String> ALL_CLASSES;

  private static List<String> CLASSES;
  protected static List<String> GENDERS;
  protected static List<String> RACES;
  protected static List<String> ALIGNMENTS;
  protected static List<String> STATUSES;
  private static List<String> CURRENCY;
  private static List<String> STAT_KEYS;

  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
    }
  };

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public String setNameLengthFieldName() {
    return "NameLength";
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    JSONObject format = loader.getFormat("Faerun");
    ALIGNMENTS = loader.getList(format.getString("Alignment"));
    ALL_CLASSES = loader.getList(format.getString("All_Classes"));
    CLASSES = loader.getList(format.getString("Classes"));
    GENDERS = loader.getList(format.getString("Gender"));
    CURRENCY = loader.getList(format.getString("Money"));
    RACES = loader.getList(format.getString("Races"));
    STAT_KEYS = loader.getList(format.getString("Stats"));
    STATUSES = loader.getList(format.getString("Status"));
  }
}
