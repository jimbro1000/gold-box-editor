/**
 * Domain classes used to identify game specific formats.
 * <p>
 * These classes define the format and fields of each recognised format
 * </p>
 *
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
package uk.org.thelair.goldbox.formats;