package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.*;

/**
 * @author Julian Brown
 * @version 1.0.0-2
 * @since 0.0.0
 */
public class SavageTwo extends SavageFrontier {
  private static SavageTwo instance;

  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private static List<String> CURRENCY = new ArrayList<String>() {
    {
      add("Platinum");
      add("Gems");
      add("Jewelry");
    }
  };

  private SavageTwo() {
    //null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Treasures of the Savage Frontier game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new SavageTwo();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Treasures of the Savage Frontier";
  }

  @Override
  public Long getFileLength() {
    return 509L;
  }

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("TreasuresOfTheSavageFrontier");
    loadFieldsFromConfig(loader, format);
  }
}
