package uk.org.thelair.goldbox.formats;

import org.json.JSONObject;
import uk.org.thelair.util.FormatLoader;

import java.util.*;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class FaerunFour extends Faerun {
  private static FaerunFour instance;

  private static final Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private FaerunFour() {
    //hidden constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Pools of Darkness game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new FaerunFour();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Pools of Darkness";
  }

  @Override
  public Long getFileLength() {
    return 510L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("PoolsOfDarkness");
    loadFieldsFromConfig(loader, format);
  }
}
