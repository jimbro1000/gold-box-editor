package uk.org.thelair.goldbox.formats;

import java.util.List;

public final class FieldDefinitionFactory {

  private static final String NUMBER_FIELD = "Number";
  private static final String STRING_FIELD = "String";
  private static final String CONSTRAINED_FIELD = "Constrained";
  private static final String ARRAY_FIELD = "Array";

  private FieldDefinitionFactory() {
    // hidden constructor
  }

  public static FieldDefinition getFieldDefinition(int fieldOffset, int fieldLength, String fieldType, List<String> fieldList, Integer minValue, Integer maxValue) {
    FieldDefinition fd;
    switch (fieldType) {
      case NUMBER_FIELD:
        if (minValue == null && maxValue == null) {
          fd = new FieldDefinition(fieldOffset, fieldLength, FieldDefinition.NUMBER_TYPE);
        } else {
          assert minValue != null;
          assert maxValue != null;
          fd = new FieldDefinition(fieldOffset, fieldLength, FieldDefinition.NUMBER_TYPE, minValue, maxValue);
        }
        break;
      case STRING_FIELD:
        fd = new FieldDefinition(fieldOffset, fieldLength, FieldDefinition.STRING_TYPE);
        break;
      case CONSTRAINED_FIELD:
        fd = new FieldDefinition(fieldOffset, fieldLength, FieldDefinition.CONSTRAINED_TYPE, fieldList);
        break;
      case ARRAY_FIELD:
        fd = new FieldDefinition(fieldOffset, fieldLength, FieldDefinition.ARRAY_TYPE);
        break;
      default:
        throw new IllegalArgumentException(fieldType + " is not a valid field type");
    }
    return fd;
  }
}
