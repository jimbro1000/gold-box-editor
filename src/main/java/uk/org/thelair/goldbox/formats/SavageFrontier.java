package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SavageFrontier implements GameFormat {
  private static List<String> ALIGNMENTS;
  private static List<String> ALL_CLASSES;
  private static List<String> CURRENCY;
  private static List<String> CLASSES;
  private static List<String> GENDERS;
  private static List<String> RACES;
  private static List<String> STAT_KEYS;
  private static List<String> STATUS;
  private static Map<String, String> STATS = new HashMap<String, String>() {
    {
      put("Strength", "Str");
      put("Intelligence", "Int");
      put("Wisdom", "Wis");
      put("Dexterity", "Dex");
      put("Constitution", "Con");
      put("Charisma", "Cha");
    }
  };

  @Override
  public List<String> getCurrencyKeys() {
    return CURRENCY;
  }

  @Override
  public List<String> getClassKeys() {
    return CLASSES;
  }

  @Override
  public List<String> getStatKeys() {
    return STAT_KEYS;
  }

  @Override
  public Map<String, String> getStatFields() {
    return STATS;
  }

  @Override
  public String setNameLengthFieldName() {
    return "NameLength";
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    JSONObject format = loader.getFormat("SavageFrontier");
    ALIGNMENTS = loader.getList(format.getString("Alignment"));
    ALL_CLASSES = loader.getList(format.getString("All_Classes"));
    CLASSES = loader.getList(format.getString("Classes"));
    GENDERS = loader.getList(format.getString("Gender"));
    CURRENCY = loader.getList(format.getString("Money"));
    RACES = loader.getList(format.getString("Races"));
    STAT_KEYS = loader.getList(format.getString("Stats"));
    STATUS = loader.getList(format.getString("Status"));
  }
}
