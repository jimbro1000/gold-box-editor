package uk.org.thelair.goldbox.formats;

import uk.org.thelair.util.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
public final class GameFormatFactory {
  private static FormatLoader loader;
  /**
   * Define map of filelengths to game formats.
   */
  private static final Map<Long, GameFormat> FORMATS =
      new HashMap<Long, GameFormat>() {
        {
          put(FaerunOne.getInstance().getFileLength(),
              FaerunOne.getInstance());
          put(FaerunTwo.getInstance().getFileLength(),
              FaerunTwo.getInstance());
          put(FaerunThree.getInstance().getFileLength(),
              FaerunThree.getInstance());
          put(FaerunFour.getInstance().getFileLength(),
              FaerunFour.getInstance());
          put(KrynnOne.getInstance().getFileLength(),
              KrynnOne.getInstance());
          put(KrynnTwo.getInstance().getFileLength(),
              KrynnTwo.getInstance());
          put(KrynnThree.getInstance().getFileLength(),
              KrynnThree.getInstance());
          put(SavageOne.getInstance().getFileLength(),
              SavageOne.getInstance());
          put(SavageTwo.getInstance().getFileLength(),
              SavageTwo.getInstance());
          put(UnlimitedAdventures.getInstance().getFileLength(),
              UnlimitedAdventures.getInstance());
          put(BuckRogers.getInstance().getFileLength(),
              BuckRogers.getInstance());
        }
      };

  /**
   * Hidden constructor for utility class.
   */
  private GameFormatFactory() {
    // hidden constructor
  }

  private static void bootstrapLoader() {
    loader = new FormatLoader();
    BuckRogers.getInstance().loadFromConfig(loader);
    FaerunOne.getInstance().loadFromConfig(loader);
    FaerunTwo.getInstance().loadFromConfig(loader);
    FaerunThree.getInstance().loadFromConfig(loader);
    FaerunFour.getInstance().loadFromConfig(loader);
    KrynnOne.getInstance().loadFromConfig(loader);
    KrynnTwo.getInstance().loadFromConfig(loader);
    KrynnThree.getInstance().loadFromConfig(loader);
    SavageOne.getInstance().loadFromConfig(loader);
    SavageTwo.getInstance().loadFromConfig(loader);
    UnlimitedAdventures.getInstance().loadFromConfig(loader);
  }

  /**
   * Identify a game format from the file length.
   *
   * @param fileLength number of bytes of given file
   * @return positively identified format or null if no match found
   */
  public static GameFormat getGameFormat(final Long fileLength) {
    if (loader == null) {
      bootstrapLoader();
    }
    return FORMATS.getOrDefault(fileLength, null);
  }
}
