package uk.org.thelair.goldbox.formats;

import java.util.*;

public class FieldDefinition {
  public static final int STRING_TYPE = 1;
  public static final int NUMBER_TYPE = 2;
  public static final int ARRAY_TYPE = 3;
  public static final int CONSTRAINED_TYPE = 4;
  public static final int MAX_NUMBER1 = 255;
  public static final int MAX_NUMBER2 = 65535;

  public int byteCount;
  public int byteOffset;
  public int byteOffsetEnd;
  public int fieldType;
  public int fieldMinimum;
  public int fieldMaximum;

  public List<String> fieldConstraints;

  public FieldDefinition(int offset, int length, int type) {
    byteOffset = offset;
    byteOffsetEnd = offset + length - 1;
    byteCount = length;
    fieldType = type;
    fieldMinimum = -1;
    fieldMaximum = -1;
    fieldConstraints = null;
  }

  public FieldDefinition(int offset, int length, int type, int min, int max) {
    byteOffset = offset;
    byteOffsetEnd = offset + length - 1;
    byteCount = length;
    fieldType = type;
    fieldMinimum = min;
    fieldMaximum = max;
    fieldConstraints = null;
  }

  public FieldDefinition(int offset, int length, int type, List<String> constraints) {
    byteOffset = offset;
    byteOffsetEnd = offset + length - 1;
    byteCount = length;
    fieldType = type;
    fieldMinimum = -1;
    fieldMaximum = -1;
    fieldConstraints = constraints;
  }

  public boolean inRange(final int offset) {
    return offset >= byteOffset && offset <= byteOffsetEnd;
  }
}
