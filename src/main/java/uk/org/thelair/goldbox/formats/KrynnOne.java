package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class KrynnOne extends Krynn {
  private static KrynnOne instance;

  private final static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private KrynnOne() {
    // null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Champions of Krynn game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new KrynnOne();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Champions Of Krynn";
  }

  @Override
  public Long getFileLength() {
    return 409L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("ChampionsOfKrynn");
    loadFieldsFromConfig(loader, format);
  }
}