package uk.org.thelair.goldbox.formats;

import org.json.*;
import uk.org.thelair.util.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class SavageOne extends SavageFrontier {
  private static SavageOne instance;

  private static Map<String, FieldDefinition> FIELD_OFFSETS = new HashMap<>();

  private SavageOne() {
    //null constructor
  }

  /**
   * Get instance for singleton.
   *
   * @return Gateway to the Savage Frontier game format
   */
  public static GameFormat getInstance() {
    if (null == instance) {
      instance = new SavageOne();
    }
    return instance;
  }

  @Override
  public String getFormatName() {
    return "Gateway to the Savage Frontier";
  }

  @Override
  public Long getFileLength() {
    return 421L;
  }

  @Override
  public Set<String> getFieldKeys() {
    return FIELD_OFFSETS.keySet();
  }

  @Override
  public Map<String, FieldDefinition> getFields() {
    return FIELD_OFFSETS;
  }

  @Override
  public void loadFromConfig(FormatLoader loader) {
    super.loadFromConfig(loader);
    JSONObject format = loader.getFormat("GatewayToTheSavageFrontier");
    loadFieldsFromConfig(loader, format);
  }
}
