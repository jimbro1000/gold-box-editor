package uk.org.thelair.goldbox;

import uk.org.thelair.goldbox.formats.FieldDefinition;
import uk.org.thelair.goldbox.formats.GameFormat;
import uk.org.thelair.goldbox.formats.GameFormatFactory;
import uk.org.thelair.util.BinaryFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.*;

/**
 * @author Julian Brown
 * @version 1.0.1-3
 * @since 0.0.0
 */
public class GoldBoxCharacter {
  /**
   * Trigger value for percentage strength value.
   */
  public static final int MAX_STRENGTH = 18;
  /**
   * Display name of strength statistic.
   */
  private static final String STRENGTH_STAT = "Strength";
  /**
   * Display name of original version of any statistic.
   */
  private static final String CURRENT_STAT = "Current";
  /**
   * Display name of current version of any statistic.
   */
  private static final String ORIGINAL_STAT = "Original";

  /**
   * The specific game save format needed for the loaded character.
   */
  private final GameFormat characterFormat;
  /**
   * The binary representation of the loaded character.
   */
  private final BinaryFile characterArray;
  /**
   * The file reference to the loaded character.
   */
  private final File characterFile;

  /**
   * Public constructor for character representation.
   *
   * @param file       source file reference
   * @param binaryFile binary representation of file content
   * @throws IOException when the targeted file fails to match a known format
   */
  public GoldBoxCharacter(
      final File file,
      final BinaryFile binaryFile
  ) throws IOException {
    characterArray = binaryFile;
    characterFile = file;
    characterFormat = GameFormatFactory.getGameFormat(characterFile.length());
    if (null == characterFormat) {
      throw new IOException("Illegal File Format");
    }
  }

  public GoldBoxCharacter() {
    characterArray = null;
    characterFile = null;
    characterFormat = null;
  }

  /**
   * Imports character file into binary representation.
   *
   * @throws IOException when the targeted file fails to load
   */
  public void load() throws IOException {
    characterArray.load(characterFile);
  }

  /**
   * Rewrites character file from binary representation.
   *
   * @throws IOException when the save fails
   */
  public void save() throws IOException {
    characterArray.write();
  }

  /**
   * Generates a hex representation map of the binary representation.
   *
   * @return hex formatted string
   */
  public String formattedToString() {
    return characterArray.toHexString();
  }

  /**
   * Modifies the named field to the given value.
   *
   * @param fieldName name of field to modify
   * @param value     updated value to use
   */
  public String setField(final String fieldName, final String value) {
    if (isValidField(fieldName)) {
      FieldDefinition field = characterFormat.getFields().get(fieldName);
      if (field.fieldType == FieldDefinition.NUMBER_TYPE) {
        return updateNumericField(fieldName, field, value);
      }
      if (field.fieldType == FieldDefinition.STRING_TYPE) {
        //must be 'name' - there aren't any other strings
        return updateStringField(fieldName, field, value);
      }
      if (field.fieldType == FieldDefinition.CONSTRAINED_TYPE) {
        return updateConstrainedField(fieldName, field, value);
      }
      return "";
    } else {
      return "Invalid field name";
    }
  }

  public String getFieldValue(final String fieldName) {
    if (!isValidField(fieldName)) {
      throw new IllegalArgumentException(fieldName + " is not a valid field name");
    }
    FieldDefinition field = characterFormat.getFields().get(fieldName);
    if (field.fieldType == FieldDefinition.CONSTRAINED_TYPE) {
      return lookupValue(fieldName, field);
    }
    return "" + getField(fieldName);
  }

  private String lookupValue(final String fieldName, final FieldDefinition field) {
    return field.fieldConstraints.get((int)getField(fieldName));
  }

  private String updateStringField(final String name, final FieldDefinition field, final String value) {
    if (!name.equals("Name")) {
      return "Cannot handle this field (yet)";
    }
    final String nameLengthField = characterFormat.setNameLengthFieldName();
    final int length = value.length();
    if (!"".equals(nameLengthField)) {
      updateNumericField(nameLengthField, characterFormat.getFields().get(nameLengthField), "" + length);
    }
    final byte[] valueBytes = value.getBytes();
    for (int i = 0; i < field.byteCount; ++i) {
      if (i < length) {
        characterArray.poke(valueBytes[i], field.byteOffset + i);
      } else {
        characterArray.poke((byte) 0, field.byteOffset + i);
      }
    }
    return name + " set to: " + getName();
  }

  private String updateNumericField(final String name, final FieldDefinition field, final String value) {
    long number = Integer.parseInt(value);
    if (number < 0) number = 0;
    if (field.fieldMinimum != -1 || field.fieldMaximum != -1) {
      if (number < field.fieldMinimum) number = field.fieldMinimum;
      if (number > field.fieldMaximum) number = field.fieldMaximum;
    }
    switch (field.byteCount) {
      case 1:
        if (number > FieldDefinition.MAX_NUMBER1) number = FieldDefinition.MAX_NUMBER1;
        characterArray.poke((byte)number, field.byteOffset);
        break;
      case 2:
        if (number > FieldDefinition.MAX_NUMBER2) number = FieldDefinition.MAX_NUMBER2;
        characterArray.pokeWord((int) number, field.byteOffset);
        break;
      default:
        characterArray.pokeDoubleWord(number, field.byteOffset);
    }
    return name + " set to: " + number;
  }

  private String updateConstrainedField(final String name, final FieldDefinition field, final String value) {
    if (field.fieldConstraints.contains(value)) {
      final int fullMatch = field.fieldConstraints.indexOf(value);
      updateNumericField(name, field, "" + fullMatch);
      return name + " set to " + value;
    }
    final String lowerValue = value.toLowerCase();
    List<String> matches = new ArrayList<>();
    for (String item : field.fieldConstraints) {
      if (item.toLowerCase().startsWith(lowerValue)) {
        matches.add(item);
      }
    }
    if (matches.size() > 1) {
      return partialMatch(matches, value);
    }
    if (matches.size() == 1) {
      final int partMatch = field.fieldConstraints.indexOf(matches.get(0));
      updateNumericField(name, field, "" + partMatch);
      return name + " set to " + matches.get(0);
    }
    return noMatch(field.fieldConstraints, value);
  }

  private String partialMatch(final List<String> matches, final String value) {
    return "Failed to match " + value + " with known valid options\n" +
        "Partial matches are:\n" +
        listValues(matches);
  }

  private String noMatch(final List<String> values, final String value) {
    return "Failed to match " + value + " with known valid options\n" +
        "Available options are:\n" +
        listValues(values);
  }

  private String listValues(final List<String> values) {
    StringBuilder result = new StringBuilder();
    for (String item : values) {
      result.append("  ").append(item).append("\n");
    }
    return result.toString();
  }

  /**
   * finds fields matching the given field name
   *
   * @param fieldName full or partial field name to match for
   * @return formatted list of matched fields
   */
  public String showFields(final String fieldName) {
    final List<String> keys = characterFormat
        .getFieldKeys()
        .stream()
        .filter(f -> f.toLowerCase().startsWith(fieldName))
        .sorted()
        .collect(Collectors.toList());
    if (keys.size() == 0) {
      return "No matches found";
    }
    StringBuilder result = new StringBuilder();
    for (String field : keys) {
      FieldDefinition fieldDefinition = characterFormat.getFields().get(field);
      result.append(field).append(" (");
      result.append(fieldDefinition.byteOffset).append(")\n");
    }
    return result.toString();
  }

  /**
   * Modifies the given save file byte to a new value.
   *
   * @param offset byte offset from file origin
   * @param value  byte value to use
   * @return outcome
   */
  public String updateByte(final String offset, final String value) {
    final int byteOffset = Integer.parseInt(offset);
    int number = Integer.parseInt(value);
    if (number > FieldDefinition.MAX_NUMBER1) number = FieldDefinition.MAX_NUMBER1;
    characterArray.poke((byte) number, byteOffset);
    String byteField = characterFormat.offsetToField(byteOffset);
    if (byteField.equals("")) {
      return "Byte at offset " + byteOffset + " set to " + number;
    }
    return "Byte at offset " + byteOffset + " (" + byteField + ") set to " + number;
  }

  /**
   * Fetches the character name field and truncates to the fixed field length.
   *
   * @return character name
   */
  public String getName() {
    final byte[] nameArray = characterArray.extractString(
        characterFormat.getFields().get("Name")
    );
    final String name = new String(nameArray, StandardCharsets.UTF_8);
    final int nameLength = characterArray.peek(
        characterFormat.getFields().get("NameLength")
    );
    return name.substring(0, nameLength);
  }

  /**
   * Fetches the named numeric field.
   *
   * @param fieldName name of field to retrieve
   * @return integer representation of the extracted field
   */
  public long getField(final String fieldName) {
    if (characterFormat.getFields().containsKey(fieldName)) {
      return getNumberField(characterFormat.getFields().get(fieldName));
    } else {
      throw new IllegalArgumentException("Illegal Field Name: " + fieldName);
    }
  }

  public long getNumberField(final FieldDefinition field) {
    long result = 0L;
    long shift = 0;
    for (int i = 0; i < field.byteCount; ++i) {
      result += ((long)characterArray.peek(field.byteOffset + i)) << shift;
      shift += 8;
    }
    return result;
  }

  public Set<String> getFields() {
    return characterFormat.getFieldKeys();
  }

  /**
   * Identifies the list of classes used by the loaded character.
   *
   * @return string of classes separated by a slash
   */
  public String getCharacterClasses() {
    String classesResult = " ";
    for (String className : characterFormat.getClassKeys()) {
      long classLevel = getField("Level" + className);
      if (classLevel > 0) {
        classesResult = classesResult.concat(className + "/");
      }
    }
    return classesResult.substring(0, classesResult.length() - 1);
  }

  /**
   * Returns the class levels for each one used by the loaded character.
   *
   * @return string of class levels separated by a slash
   */
  public String getCharacterLevels() {
    String levelResult = " ";
    for (String className : characterFormat.getClassKeys()) {
      long classLevel = getField("Level" + className);
      if (classLevel > 0) {
        levelResult = levelResult.concat(classLevel + "/");
      }
    }
    return levelResult.substring(0, levelResult.length() - 1);
  }

  /**
   * Constructs a string representing all the stats for the loaded character.
   *
   * @return formatted string of character stats
   */
  private String getCharacterStats() {
    String result = "";
    Map<String, String> stats = characterFormat.getStatFields();
    for (String statName : characterFormat.getStatKeys()) {
      long current = getField(stats.get(statName) + CURRENT_STAT);
      long original = getField(stats.get(statName) + ORIGINAL_STAT);
      result = result.concat(statName + ": ");
      String currentResult = "" + current;
      String originalResult = "" + original;
      if (STRENGTH_STAT.equals(statName)) {
        long extendCurrent = getField(
            stats.get(statName) + "Ext" + CURRENT_STAT
        );
        long extendOriginal = getField(
            stats.get(statName) + "Ext" + ORIGINAL_STAT
        );
        if (current == MAX_STRENGTH) {
          currentResult = currentResult.concat("(" + extendCurrent + ")");
        }
        if (original == MAX_STRENGTH) {
          originalResult = originalResult.concat("(" + extendOriginal + ")");
        }
      }
      result = result.concat(currentResult);
      if (!currentResult.equals(originalResult)) {
        result = result.concat(" / " + originalResult);
      }
      result = result.concat("\n");
    }
    return result;
  }

  /**
   * Constructs a string representing the currency for the loaded character.
   *
   * @return formatted string of currency
   */
  private String getMoney() {
    String result = "";
    for (String coin : characterFormat.getCurrencyKeys()) {
      result = result.concat(coin + ": " + getField(coin) + "\n");
    }
    return result;
  }

  /**
   * Constructs a string representing the loaded characters essential details.
   *
   * @return formatted string
   */
  public String getCharacterSheet() {
    String result = characterFormat.getFormatName() + "\n";
    result = result.concat("Name: " + getName() + "\n");
    result = result.concat("Class: " + getCharacterClasses() + " ");
    result = result.concat("Level: " + getCharacterLevels() + "\n\n");
    result = result.concat(getCharacterStats() + "\n");
    result = result.concat(getMoney() + "\n");
    return result;
  }

  /**
   * Identifies the named game file format.
   *
   * @return name of game format used
   */
  public String getFormatName() {
    return characterFormat.getFormatName();
  }

  private boolean isValidField(String fieldName) {
    return this.getFields().contains(fieldName);
  }

  public String getOffset(int offset) {
    if (offset > characterFormat.getFileLength()) {
      return "Invalid offset";
    }
    final String fieldName = characterFormat.offsetToField(offset);
    if (fieldName.equals("")) {
      return "" + characterArray.peek(offset);
    }
    if ("Name".equals(fieldName)) {
      return fieldName + ": " + getName();
    } else {
      return fieldName + ": " + getFieldValue(fieldName);
    }
  }
}
