/**
 * Domain classes used to produce a command line editor application.
 * <p>
 * These classes contain the application entry point only.
 * </p>
 *
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
package uk.org.thelair;