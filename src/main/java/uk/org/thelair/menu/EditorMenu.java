package uk.org.thelair.menu;

import asg.cliche.ShellFactory;
import uk.org.thelair.goldbox.GoldBoxCharacterBuilder;
import uk.org.thelair.util.ByteToFileWriter;
import uk.org.thelair.util.FileHandler;
import uk.org.thelair.util.FileToByteReader;
import uk.org.thelair.util.PathHelper;

import java.io.IOException;

public class EditorMenu {
  /**
   * Constructor for editor menu loop.
   *
   * @throws IOException when the command loop fails
   */
  public EditorMenu(final FileHandler fileHandler, PathHelper pathHelper) throws IOException {
    GoldBoxCharacterBuilder builder = new GoldBoxCharacterBuilder(
        new FileToByteReader(),
        new ByteToFileWriter());
    ShellFactory.createConsoleShell(
        "Action ",
        "Gold Box Editor",
        new Editor(builder, fileHandler, pathHelper)
    ).commandLoop();
  }
}
