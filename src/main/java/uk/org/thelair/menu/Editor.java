package uk.org.thelair.menu;

import asg.cliche.Command;
import asg.cliche.Param;
import uk.org.thelair.goldbox.GoldBoxCharacter;
import uk.org.thelair.goldbox.GoldBoxCharacterBuilder;
import uk.org.thelair.goldbox.NullGoldBoxCharacter;
import uk.org.thelair.util.FileHandler;
import uk.org.thelair.util.PathHelper;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.*;

public class Editor {
  private static final NullGoldBoxCharacter PLACE_HOLDER = new NullGoldBoxCharacter();
  private GoldBoxCharacter activeCharacter = PLACE_HOLDER;
  private final GoldBoxCharacterBuilder builder;
  private final FileHandler fileHandler;
  private final PathHelper pathHelper;
  private String path;

  private static final Pattern NUMBER_REGEX = Pattern.compile("^\\d+$");

  /**
   * Constructor.
   *
   * @param goldBoxCharacterBuilder injected character builder
   * @param fileHandlerInstance     injected file handler
   * @param helperInstance          injected path helper
   */
  public Editor(GoldBoxCharacterBuilder goldBoxCharacterBuilder,
                FileHandler fileHandlerInstance,
                PathHelper helperInstance) {
    builder = goldBoxCharacterBuilder;
    fileHandler = fileHandlerInstance;
    pathHelper = helperInstance;
    if (pathHelper.isWindowsOs()) {
      path =
          "C:\\Program Files (x86)\\GOG Galaxy\\Games\\Champions of Krynn\\cloud_saves\\SAVE";
    } else {
      path = "/Library";
    }
  }

  private boolean isNumberString(String value) {
    return NUMBER_REGEX.matcher(value).find();
  }

  /**
   * Change directory command.
   *
   * @param pathDelta absolute or relative file path
   * @return confirmation string of action
   */
  @Command(description = "Change working directory")
  public String cd(
      @Param(name = "path", description = "file path") String pathDelta
  ) {
    try {
      path = pathHelper.resolveChangeToPath(path, pathDelta);
      return path;
    } catch (IOException ex) {
      return "Illegal directory change";
    }
  }

  /**
   * List command.
   *
   * @return list of valid files in path
   */
  @Command(description = "list valid save files in working directory")
  public String ls() {
    List<String> fileList = fileHandler.getFilesInDirectory(path);
    StringBuilder result = new StringBuilder();
    if (fileList.size() == 0) {
      result.append("No files found");
    } else {
      result.append("Files in directory:\n");
      for (String file : fileList) {
        result.append(file).append("\n");
      }
    }
    return result.toString();
  }

  /**
   * Load command.
   *
   * @param saveId character id
   * @param gameId game id
   * @return Confirmation of action
   * @throws IOException when the target file cannot be loaded
   */
  @Command(description = "load save file from working directory")
  public String load(
      @Param(name = "saveId", description = "Character Id (1-6)") int saveId,
      @Param(name = "gameId", description = "Game save set (A-G)") String gameId
  ) throws IOException {
    String loadResult;
    File saveFile = fileHandler.getFileWithId(path, saveId, gameId);
    if (saveFile != null) {
      activeCharacter = builder.buildCharacter(saveFile);
      activeCharacter.load();
      loadResult = "character loaded: "
          + activeCharacter.getFormatName() + " "
          + activeCharacter.getName();
    } else {
      //no such file
      loadResult = "Unable to find character file";
    }
    return loadResult;
  }

  /**
   * Save command.
   *
   * @return outcome
   * @throws IOException when the target file cannot be saved
   */
  @Command(description = "save changes to character file back to working directory")
  public String save() throws IOException {
    activeCharacter.save();
    return "Character file saved";
  }

  /**
   * Map character command.
   *
   * @return formatted string representing hex map of loaded character
   */
  @Command(description = "display byte map of loaded savefile")
  public String map() {
    return activeCharacter.formattedToString();
  }

  /**
   * Pwd command.
   *
   * @return Confirmation of current canonical file path
   * @throws IOException when the current path is impossible
   */
  @Command(description = "show current working directory")
  public String pwd() throws IOException {
    return pathHelper.canonicalPath(path);
  }

  /**
   * Show command displays value for a given field.
   *
   * @param fieldName Name of field to show
   * @return value of field
   */
  @Command(description = "show value of named field, will list valid fields if passed '?'")
  public String show(
      @Param(name = "field", description = "field name to examine") String fieldName
  ) {
    if (fieldName.equals("?")) {
      return getPossibleFields();
    } else {
      if (isNumberString(fieldName)) {
        return activeCharacter.getOffset(Integer.parseInt(fieldName));
      } else {
        if ("Name".equals(fieldName)) {
          return activeCharacter.getName();
        } else {
          return getSpecificField(fieldName);
        }
      }
    }
  }

  /**
   * Fields command displays value for a given field.
   *
   * @param partial Partial (initial) name of field to show
   * @return matching fields
   */
  @Command(description = "show list of valid fields, or fields matching the given prefix")
  public String fields(
      @Param(name = "partial", description = "partial field name to find, \"\" or all to list all fields") String partial
  ) {
    final String partialField = partial.trim().toLowerCase();
    if (!"".equals(partialField) && !"all".equals(partialField)) {
      return activeCharacter.showFields(partialField);
    } else {
      return activeCharacter.showFields("");
    }
  }

  /**
   * Print command displays all fields for the loaded character
   *
   * @return formatted character sheet
   */
  @Command(description = "show formatted character sheet for loaded savefile")
  public String print() {
    return activeCharacter.getCharacterSheet();
  }

  /**
   * Modify a single field value for the loaded character
   *
   * @param fieldName name of field
   * @param value target value for set operation
   * @return outcome
   */
  @Command(description = "update value of named field")
  public String set(
      @Param(name = "field name", description = "field name to modify") String fieldName,
      @Param(name = "value", description = "target value") String value
  ) {
    return activeCharacter.setField(fieldName, value);
  }

  @Command(description = "Update value of byte at given offset")
  public String byteset(
      @Param(name = "offset", description = "byte offset in save file") String offset,
      @Param(name = "value", description = "target value") String value
  ) {
    return activeCharacter.updateByte(offset, value);
  }

  private String getPossibleFields() {
    StringBuilder builder = new StringBuilder("Available fields:\n");
    for (String field : activeCharacter.getFields()) {
      builder.append(field).append("\n");
    }
    return builder.toString();
  }

  private String getSpecificField(String fieldName) {
    try {
      final String value = activeCharacter.getFieldValue(fieldName);
      return fieldName + ": " + value;
    } catch (IllegalArgumentException ex) {
      return fieldName + " is not a valid field name";
    }
  }
}
