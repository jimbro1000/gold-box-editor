/**
 * Domain classes used to manage the user interaction
 * <p>
 * These classes contain the editor logic
 * </p>
 *
 * @author Julian Brown
 * @version 1.0.0-0
 * @since 0.0.0
 */
package uk.org.thelair.menu;