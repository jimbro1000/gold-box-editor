package uk.org.thelair.menu;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import uk.org.thelair.goldbox.*;
import uk.org.thelair.util.*;

import java.io.*;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EditorTest {
  //  GoldBoxCharacterBuilder characterBuilder;
  @Mock
  PathHelper pathHelper;
  @Mock
  FileHandler fileHandler;
  @Mock
  GoldBoxCharacter activeCharacter;
  @Mock
  GoldBoxCharacterBuilder characterBuilder;

  @InjectMocks
  private Editor editor;

  @Test
  @DisplayName("cd changes the current path")
  public void cdChangesPath() throws IOException {
    final String before = "/Library";
    final String delta = "games";
    final String expected = before.concat("/").concat(delta);
    lenient().doReturn(false).when(pathHelper).isWindowsOs();
    doReturn(expected).when(pathHelper).resolveChangeToPath(before, delta);
    String actual = editor.cd(delta);
    Assertions.assertEquals(actual, expected);
  }

  @Test
  public void cdReturnsAWarningIfThePathCannotBeResolved() throws IOException {
    doThrow(IOException.class).when(pathHelper).resolveChangeToPath(isA(String.class), isA(String.class));
    Assertions.assertEquals("Illegal directory change", editor.cd("illegal"));
  }

  @Test
  public void lsListsValidFilesInPath() {
    final String filename = "file1.sav";
    final List<String> fileList = new ArrayList<>();
    fileList.add(filename);
    doReturn(fileList).when(fileHandler).getFilesInDirectory(any());
    String actual = editor.ls();
    Assertions.assertTrue(actual.indexOf(filename) > 0);
  }

  @Test
  public void lsReturnsNoFilesFoundIfNoFilesFoundInPath() {
    final List<String> fileList = new ArrayList<>();
    doReturn(fileList).when(fileHandler).getFilesInDirectory(any());
    String actual = editor.ls();
    Assertions.assertEquals("No files found", actual);
  }

  @Test
  public void loadBuildsACharacterFromFile() throws IOException {
    File dummyFile = new File("dummy");

    doReturn(dummyFile).when(fileHandler).getFileWithId(isA(String.class), isA(int.class), isA(String.class));
    doReturn(activeCharacter).when(characterBuilder).buildCharacter(dummyFile);
    doNothing().when(activeCharacter).load();
    doReturn("Test Format").when(activeCharacter).getFormatName();
    doReturn("Test Name").when(activeCharacter).getName();

    String actual = editor.load(1, "A");
    String expected = "character loaded: Test Format Test Name";
    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void loadReportsUnableToFindANonExistentFile() throws IOException {
    File dummyFile = null;
    doReturn(dummyFile).when(fileHandler).getFileWithId(isA(String.class), isA(int.class), isA(String.class));
    String actual = editor.load(1, "A");
    String expected = "Unable to find character file";
    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void pwdReturnsTheCurrentDirectoryPath() throws IOException {
    doReturn("path").when(pathHelper).canonicalPath(isA(String.class));
    Assertions.assertEquals("path", editor.pwd());
  }

  @Test
  public void mapReturnsTheHexMapRepresentationOfTheLoadedCharacter() throws IOException {
    String expected = "hex map";
    File dummyFile = new File("dummy");

    doReturn(dummyFile).when(fileHandler).getFileWithId(isA(String.class), isA(int.class), isA(String.class));
    doReturn(activeCharacter).when(characterBuilder).buildCharacter(dummyFile);
    doReturn(expected).when(activeCharacter).formattedToString();
    doNothing().when(activeCharacter).load();
    editor.load(1, "A");

    String result = editor.map();
    Assertions.assertEquals(expected, result);
  }

  @Test
  public void mapReturnsWarningIfACharacterIsNotLoaded() {
    String expected = "No character loaded";

    String result = editor.map();
    Assertions.assertEquals(expected, result);
  }

  @Test
  @Disabled
  public void showDisplaysASingleField() throws IOException {
    File dummyFile = Mockito.mock(File.class);
    String fieldName = "StrOriginal";
    int expectedValue = 12;
    String expectedResult = fieldName + ": " + expectedValue;

    doReturn("dummyFile").when(dummyFile).getName();
    doReturn(409L).when(dummyFile).length();
    doReturn(dummyFile).when(fileHandler).getFileWithId(isA(String.class), isA(int.class), isA(String.class));
    doReturn(activeCharacter).when(characterBuilder).buildCharacter(dummyFile);
    doReturn(expectedValue).when(activeCharacter).getField(fieldName);
    doNothing().when(activeCharacter).load();

    editor.load(1, "A");
    String result = editor.show(fieldName);

    Assertions.assertEquals(expectedResult, result);
  }

  // test show lists fields if parameter is "?"

  // test fields list all fields if parameter is "" or "all"
  // test fields list fields starting with parameter
}
