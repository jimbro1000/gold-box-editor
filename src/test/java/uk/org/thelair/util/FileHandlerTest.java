package uk.org.thelair.util;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import java.io.*;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileHandlerTest {
  private static final String GAME_FILE_NAME_1 = "file1";
  private static final String GAME_FILE_NAME_2 = "file2";
  @Mock
  SafeFileUtility fileUtility;
  private FileHandler fileHandler;
  private FilenameFilter fileFilter;
  private File gameFile1;
  private File gameFile2;

  @BeforeEach
  public void setup() {
    fileFilter = (dir, name) -> {
      String lowercaseName = name.toLowerCase();
      return lowercaseName.endsWith(".sav");
    };
    fileHandler = new FileHandler(fileUtility, fileFilter);
    gameFile1 = new File(GAME_FILE_NAME_1);
    gameFile2 = new File(GAME_FILE_NAME_2);
  }

  @Test
  @DisplayName("Given a valid path getFilesInDirectory returns the filtered list of game files")
  public void getFilesInDirectoryListsFilteredGameFiles() {
    final String filePath = "~/savefiles";
    final List<File> fileList = new ArrayList<>();
    fileList.add(gameFile1);
    fileList.add(gameFile2);

    when(fileUtility.getFilesInDirectory(filePath, fileFilter)).thenReturn(fileList);

    List<String> result = fileHandler.getFilesInDirectory(filePath);

    verify(fileUtility, times(1)).getFilesInDirectory(filePath, fileFilter);
    assertThat(result, contains(GAME_FILE_NAME_1, GAME_FILE_NAME_2));
  }

  @Test
  @DisplayName("Given a valid path and file description getFile returns the specified file reference")
  public void getFileReturnsTheSpecifiedFile() {
    final String filePath = "~/savefile";
    final String gameId = "A";
    final int characterId = 1;
    final String filename = String.format("CHRDAT" + gameId + "%d.SAV", characterId);

    when(fileUtility.getFile(filePath, filename)).thenReturn(gameFile1);

    Assertions.assertEquals(gameFile1, fileHandler.getFileWithId(filePath, characterId, gameId));
    verify(fileUtility, times(1)).getFile(filePath, filename);
  }
}
