package uk.org.thelair.util;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import uk.org.thelair.goldbox.formats.*;

import java.io.*;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class BinaryFileTest {
  @Mock
  File mockFile;
  @Mock
  FileReader mockFileReader;
  @Mock
  FileWriter mockFileWriter;

  int fileLength;
  byte[] loadResult;

  @BeforeEach
  public void setup() throws IOException {
    loadResult = new byte[]{1, 3, 5, 7, 9, 11, 13, 15, 17, 19};
    fileLength = loadResult.length;
    when(mockFileReader.read(mockFile)).thenReturn(loadResult);
  }

  @Test
  @DisplayName("Given a valid file reference load reads and stores the specified file")
  public void loadReadsTheSpecifiedFile() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertEquals(loadResult, binaryFile.binaryBuffer);
  }

  @Test
  @DisplayName("Given a valid loaded file write stores the updated file")
  public void writeSaveTheSpecifiedFile() throws IOException {
    doNothing().when(mockFileWriter).write(mockFile, loadResult);

    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    binaryFile.write();
    verify(mockFileWriter, times(1)).write(mockFile, loadResult);
  }

  @Test
  @DisplayName("Given a valid loaded file size returns the size of the file")
  public void sizeReturnsTheLengthOfTheFile() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertEquals(binaryFile.size(), fileLength);
  }

  @Test
  @DisplayName("Given a valid loaded file peek returns the value at the given file offset")
  public void peekReturnsTheByteAtAGivenOffset() throws Exception {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertEquals(loadResult[5], binaryFile.peek(5));
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file peek throws an exception if the provided offset is outside of the file range")
  @ValueSource(ints = {-1, 11})
  public void peekThrowsAnExceptionIfTheOffsetIsOutOfRange(int offset) throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.peek(offset));
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file poke changes the value at the given file offset")
  @CsvSource({"2, 10", "3, 2", "6, 6"})
  public void pokeModifiesTheByteAtAGivenOffset(String offset, String value) throws Exception {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    binaryFile.poke(Byte.parseByte(value), Integer.parseInt(offset));
    Assertions.assertEquals(loadResult[Integer.parseInt(offset)], Byte.parseByte(value));
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file peek throws an exception if the provided offset is outside of the file range")
  @ValueSource(ints = {-1, 11})
  public void pokeThrowsAnExceptionIfTheOffsetIsOutOfRange(int offset) throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.poke((byte) 1, offset));
  }

  @Test
  @DisplayName("Given a valid loaded file copyBufferToArray copies the specified segment of the file to an array")
  public void copyBufferToArrayCopiesASegmentOfTheGivenFile() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    byte[] subArray = binaryFile.copyBufferToArray(1, 5);
    Assertions.assertArrayEquals(new byte[]{3, 5, 7, 9, 11}, subArray);
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file copyBufferToArray throws an exception if the provided offset is outside of the file range")
  @ValueSource(ints = {-1, 10})
  public void copyBufferToArrayThrowsAnExceptionIfTheOffsetIsOutOfRange(int offset) throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.copyBufferToArray(offset, 2));
  }

  @Test
  @DisplayName("Given a valid loaded file copyBufferToArray throws an exception if the provided offset and segment length is outside of the file range")
  public void copyBufferToArrayThrowsAnExceptionIfTheOffsetIsOutOfRange() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.copyBufferToArray(9, 2));
  }

  @Test
  @DisplayName("Given a valid loaded file copyArrayToBuffer copies the array into the file")
  public void copyArrayToBufferCopiesTheArrayIntoTheBufferAtTheGivenOffset() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    byte[] insertArray = {2, 4, 6, 8};
    binaryFile.copyArrayToBuffer(2, insertArray);
    Assertions.assertArrayEquals(new byte[]{1, 3, 2, 4, 6, 8, 13, 15, 17, 19}, binaryFile.binaryBuffer);
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file copyArrayToBuffer throws an exception if the provided offset is outside of the file range")
  @ValueSource(ints = {-1, 10})
  public void copyArrayToBufferThrowsAnExceptionIfTheOffsetIsOutOfRange(int offset) throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.copyArrayToBuffer(offset, new byte[]{}));
  }

  @Test
  @DisplayName("Given a valid loaded file copyArrayToBuffer throws an exception if the provided offset and segment length is outside of the file range")
  public void copyArrayToBufferThrowsAnExceptionIfTheOffsetIsOutOfRange() throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.copyArrayToBuffer(9, new byte[]{2, 4, 6, 8}));
  }

  @Test
  @DisplayName("Given a valid loaded file and an offset extractString returns a sub-string as a byte array")
  public void extractStringReturnsANullTerminatedStringAsAByteArray() throws IOException {
    loadResult = new byte[]{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 65, 66, 67, 0};
    byte[] expected = new byte[]{65, 66, 67};
    fileLength = loadResult.length;
    when(mockFileReader.read(mockFile)).thenReturn(loadResult);

    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);

    byte[] extract = binaryFile.extractString(new FieldDefinition(10, 3, FieldDefinition.STRING_TYPE));
    Assertions.assertArrayEquals(extract, expected);
  }

  @ParameterizedTest
  @DisplayName("Given a valid loaded file extractString throws an exception if the provided offset is outside of the file range")
  @ValueSource(ints = {-1, 10})
  public void extractStringThrowsAnExceptionIfTheOffsetIsOutOfRange(int offset) throws IOException {
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    Assertions.assertThrows(IndexOutOfBoundsException.class, () -> binaryFile.extractString(new FieldDefinition(offset, 10, FieldDefinition.STRING_TYPE)));
  }

  @Test
  public void toHexStringGeneratesHexMapOfBinaryFile() throws IOException {
    loadResult = new byte[]{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 65, 66, 67, -127, 0};
    String expected = "01030507 090b0d0f 11134142 438100";
    fileLength = loadResult.length;
    when(mockFileReader.read(mockFile)).thenReturn(loadResult);
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    String result = binaryFile.toHexString();
    Assertions.assertTrue(result.contains(expected));
  }

  @Test
  public void toHexStringProvidesAddressReferencesForFilesUpTo65k() throws IOException {
    loadResult = buildLongerByteArray(1000);
    fileLength = loadResult.length;
    when(mockFileReader.read(mockFile)).thenReturn(loadResult);
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);
    binaryFile.toHexString();
    Assertions.assertTrue(true);
  }

  @Test
  public void getNameReturnsTheFilename() throws IOException {
    String filename = "TestFileName";
    loadResult = new byte[]{0};
    when(mockFileReader.read(mockFile)).thenReturn(loadResult);
    when(mockFile.getName()).thenReturn(filename);
    BinaryFile binaryFile = new BinaryFile(mockFileReader, mockFileWriter);
    binaryFile.load(mockFile);

    Assertions.assertEquals(filename, binaryFile.getName());
  }

  private byte[] buildLongerByteArray(int length) {
    byte value = 1;
    byte[] result = new byte[length];
    for (int i = 0; i < length; ++i) {
      result[i] = value++;
    }
    return result;
  }
}
