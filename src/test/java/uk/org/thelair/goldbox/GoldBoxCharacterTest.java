package uk.org.thelair.goldbox;

import java.io.*;
import java.nio.charset.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import uk.org.thelair.goldbox.formats.*;
import uk.org.thelair.util.*;
import uk.org.thelair.util.FileReader;
import uk.org.thelair.util.FileWriter;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GoldBoxCharacterTest {
  @Mock
  File mockFile;
  @Mock
  File variableMockFile;
  @Mock
  FileReader reader;
  @Mock
  FileWriter writer;
  @Mock
  BinaryFile binaryFile;

  private GoldBoxCharacter testCharacter;

  @BeforeEach
  public void setup() throws IOException {
    whenFileFormatIsKrynnOne();
    setupCharacterTest();
    setupKrynnClass();
  }

  @ParameterizedTest
  @DisplayName("Given a valid File reference identifies the file type")
  @CsvSource({"409, Champions Of Krynn", "285, Pool Of Radiance", "397, Unlimited Adventures"})
  public void givenAValidFileItIdentifiesTheCharacterFileType(String fileSize, String expected) throws Exception {
    final long fileLength = Long.parseLong(fileSize);
    doReturn(fileLength).when(variableMockFile).length();

    GoldBoxCharacterBuilder builder = new GoldBoxCharacterBuilder(reader, writer);
    GoldBoxCharacter builtCharacter = builder.buildCharacter(variableMockFile);

    verify(variableMockFile, times(1)).length();
    Assertions.assertEquals(expected, builtCharacter.getFormatName());
  }

  @Test
  public void givenAnInvalidFileItThrowsAnIOException() {
    doReturn((long) 20).when(mockFile).length();

    Assertions.assertThrows(IOException.class, () -> {
      new GoldBoxCharacter(mockFile, binaryFile);
    });
  }

  @Test
  @DisplayName("Given a valid file reference load ingests the file")
  public void givenAValidFileItLoadsTheFile() throws IOException {
    whenFileFormatIsKrynnOne();
    setupCharacterTest();

    testCharacter.load();

    verify(binaryFile, times(1)).load(mockFile);
  }

  @Test
  @Disabled
  public void getNameExtractsNameFromTheByteArray() throws IOException {
    setupName("testname", (byte) 7);
    String actual = testCharacter.getName();
    Assertions.assertEquals("testnam", actual);
  }

  @Test
  @Disabled
  public void getCharacterClassesFindsAndReportsTheClassNames() throws IOException {
    String result = testCharacter.getCharacterClasses();
    Assertions.assertEquals("Cleric", result);
  }

  @Test
  @Disabled
  public void getCharacterLevelsFindsAndReportsTheClassLevels() throws IOException {
    String result = testCharacter.getCharacterLevels();
    Assertions.assertEquals("1", result);
  }

  @ParameterizedTest
  @DisplayName("Given a valid file getField returns the value of a single named field")
  @CsvSource({"1, LevelCleric", "0, LevelKnight"})
  @Disabled
  public void getFieldFetchesTheValueOfASingleNamedField(String expected, String fieldName) throws IOException {
    long result = testCharacter.getField(fieldName);
    Assertions.assertEquals(Integer.parseInt(expected), result);
  }

  @DisplayName("Given a valid file getField throws an IllegalArgumentException if the name field doesn't exist")
  @Test
  public void getFieldThrowsIllegalArgumentExceptionIfTheNameFieldDoesntExist() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      testCharacter.getField("Unknown");
    });
  }

  @Test
  public void setFieldUpdatesTheNamedFieldValue() {
    testCharacter.setField("any", "0");
    Assertions.assertTrue(true);
  }

  @Test
  public void saveWritesTheCharacterToDisk() throws IOException {
    testCharacter.save();
    Assertions.assertTrue(true);
  }

  @Test
  public void formattedToStringReturnsAHexStringOfTheCharacterFile() {
    String expected = "a hex string";
    doReturn(expected).when(binaryFile).toHexString();
    String actual = testCharacter.formattedToString();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Disabled
  public void getCharacterSheetReturnsAFormattedString() {
    String expectedName = "formatted";
    setupKrynnStats();
    setupKrynnMoney();
    setupName(expectedName, (byte) expectedName.length());

    String characterSheet = testCharacter.getCharacterSheet();
    Assertions.assertTrue(characterSheet.contains(expectedName));
    Assertions.assertTrue(characterSheet.contains("Cleric"));
  }

  @Test
  @Disabled
  public void getCharacterShowsExtendedStrengthValues() {
    setupKrynnStrongStats();
    setupKrynnMoney();
    setupName("strong", (byte) 6);
    String characterSheet = testCharacter.getCharacterSheet();
    Assertions.assertTrue(characterSheet.contains("Strength: 18(50)"));
  }

  @Test
  @Disabled
  public void getCharacterShowsDamagedStatValues() {
    setupKrynnHandicappedStats();
    setupKrynnMoney();
    setupName("wounded", (byte) 7);
    String characterSheet = testCharacter.getCharacterSheet();
    Assertions.assertTrue(characterSheet.contains("Strength: 16 / 17"));
  }

  private void whenFileFormatIsKrynnOne() {
    doReturn(409L).when(mockFile).length();
  }

  private void setupCharacterTest() throws IOException {
    testCharacter = new GoldBoxCharacter(mockFile, binaryFile);
  }

  private void setupKrynnClass() {
    lenient().doReturn((byte) 1).when(binaryFile).peek(249);
    for (int i = 250; i < 257; ++i) {
      lenient().doReturn((byte) 0).when(binaryFile).peek(i);
    }
  }

  private void setupKrynnStats() {
    for (int i = 16; i < 28; ++i) {
      lenient().doReturn((byte) 16).when(binaryFile).peek(i);
    }
    lenient().doReturn((byte) 0).when(binaryFile).peek(28);
    lenient().doReturn((byte) 0).when(binaryFile).peek(29);
  }

  private void setupKrynnStrongStats() {
    lenient().doReturn((byte) 18).when(binaryFile).peek(16);
    lenient().doReturn((byte) 18).when(binaryFile).peek(17);
    for (int i = 18; i < 28; ++i) {
      lenient().doReturn((byte) 16).when(binaryFile).peek(i);
    }
    lenient().doReturn((byte) 50).when(binaryFile).peek(28);
    lenient().doReturn((byte) 50).when(binaryFile).peek(29);
  }

  private void setupKrynnHandicappedStats() {
    lenient().doReturn((byte) 17).when(binaryFile).peek(16);
    lenient().doReturn((byte) 16).when(binaryFile).peek(17);
    for (int i = 18; i < 28; ++i) {
      lenient().doReturn((byte) 16).when(binaryFile).peek(i);
    }
    lenient().doReturn((byte) 0).when(binaryFile).peek(28);
    lenient().doReturn((byte) 0).when(binaryFile).peek(29);
  }

  private void setupKrynnMoney() {
    for (int i = 237; i < 248; ++i) {
      lenient().doReturn((byte) 1).when(binaryFile).peek(i);
    }
  }

  private void setupName(String characterName, byte nameLength) {
    byte[] name = characterName.getBytes(StandardCharsets.UTF_8);

    lenient().doReturn(name).when(binaryFile).extractString(isA(FieldDefinition.class));
    lenient().doReturn(nameLength).when(binaryFile).peek(0);
  }
}
