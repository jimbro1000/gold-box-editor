package uk.org.thelair.goldbox.formats;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GameFormatFactoryTest {
    @Test
    @DisplayName("Given a file of length 409 expect factory to return a Champions of Krynn formatter")
    public void givenAFileOfLength409GetGameFormatReturnsAKrynnOneFormatter() {
        long length = 409;
        GameFormat result = GameFormatFactory.getGameFormat(length);
        Assertions.assertTrue(result instanceof KrynnOne);
    }

    @Test
    @DisplayName("Given a file of unexpected length then expect factory to return NULL")
    public void givenAFileOfUnexpectedLengthGetGameFormatReturnsNull() {
        long length = 1;
        GameFormat result = GameFormatFactory.getGameFormat(length);
        Assertions.assertNull(result);
    }
}
