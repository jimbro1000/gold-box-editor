package uk.org.thelair.goldbox.formats;

import org.junit.jupiter.api.*;
import uk.org.thelair.util.*;

public class KrynnOneTest {
  private static GameFormat format = KrynnOne.getInstance();

  @BeforeAll
  public static void fetchConfig() {
    FormatLoader loader = new FormatLoader();
    format.loadFromConfig(loader);
  }

  @Test
  public void krynnOneFileLengthIs409Bytes() {
    Assertions.assertEquals(409L, format.getFileLength());
  }

  @Test
  public void krynnOneFormatNameIsChampionsOfKrynn() {
    Assertions.assertEquals("Champions Of Krynn", format.getFormatName());
  }

  @Test
  public void krynnOneHas50FieldKeys() {
    Assertions.assertEquals(50, format.getFieldKeys().size());
  }

  @Test
  public void krynnOneHas50Fields() {
    Assertions.assertEquals(50, format.getFields().size());
  }

  @Test
  public void krynnFormatsHave6CurrencyTypes() {
    Assertions.assertEquals(6, format.getCurrencyKeys().size());
  }

  @Test
  public void krynnCharactersHave6StatKeys() {
    Assertions.assertEquals(6, format.getStatKeys().size());
  }

  @Test
  public void krynnCharactersHave6Stats() {
    Assertions.assertEquals(6, format.getStatFields().size());
  }

  @Test
  public void krynnCharactersHave8Classes() {
    Assertions.assertEquals(8, format.getClassKeys().size());
  }
}
