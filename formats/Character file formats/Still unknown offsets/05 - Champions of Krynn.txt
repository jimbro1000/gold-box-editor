CHAMPIONS OF KRYNN
Unknown offsets (unknown offsets that are null for all monsters are not listed)

DECIMAL OFFSET         276  
HEXADECIMAL OFFSET     114  

AURAK                   -        MON3CHA.DAX
BAAZ                    -        MON1CHA.DAX
BAAZ                    -        MON2CHA.DAX
BAAZ                    -        MON3CHA.DAX
BLACK DRAGON            -        MON2CHA.DAX
BLACK ROBE MAGE         -        MON1CHA.DAX
BLUE DRAGON             -        MON3CHA.DAX
BOZAK                   -        MON1CHA.DAX
BOZAK                   -        MON2CHA.DAX
BOZAK                   -        MON3CHA.DAX
CLERIC                  -        MON1CHA.DAX
DRAGON MASTER           01       MON3CHA.DAX
ELVISH CURATE           -        MON2CHA.DAX
ELVISH KILLER           01       MON2CHA.DAX
ELVISH MAGE             -        MON3CHA.DAX
ELVISH ROGUE            -        MON2CHA.DAX
ENCHANTER               -        MON2CHA.DAX
ENCHANTER               -        MON3CHA.DAX
EVIL CHAMPION           01       MON2CHA.DAX
EVIL CHAMPION           01       MON3CHA.DAX
EVIL CURATE             -        MON1CHA.DAX
EVIL CURATE             -        MON2CHA.DAX
EVIL FIGHTER            01       MON2CHA.DAX
EVIL PRIEST             -        MON3CHA.DAX
GHAST                   -        MON2CHA.DAX
GHAST                   -        MON3CHA.DAX
GHAST                   -        MON3CHA.DAX
GHOUL                   -        MON1CHA.DAX
GHOUL                   -        MON2CHA.DAX
GHOUL                   -        MON3CHA.DAX
GIANT CENTIPEDE         -        MON1CHA.DAX
GIANT CENTIPEDE         -        MON2CHA.DAX
GIANT RAT               -        MON1CHA.DAX
GIANT RAT               -        MON2CHA.DAX
GIANT RAT               -        MON3CHA.DAX
GIANT SNAKE             -        MON1CHA.DAX
GIANT SNAKE             -        MON2CHA.DAX
GIANT SPIDER            -        MON1CHA.DAX
GOBLIN                  -        MON1CHA.DAX
GREEN DRAGON            -        MON2CHA.DAX
HILL GIANT              -        MON1CHA.DAX
HOBGOBLIN               -        MON1CHA.DAX
HOBGOBLIN               -        MON3CHA.DAX
HOBGOBLIN LDR           -        MON1CHA.DAX
HOBGOBLIN LDR           -        MON3CHA.DAX
KAPAK                   -        MON1CHA.DAX
KAPAK                   -        MON2CHA.DAX
KAPAK                   -        MON3CHA.DAX
KILDIRF                 -        MON1CHA.DAX
LYN SWIFTFOOT           -        MON2CHA.DAX
MAYA                    -        MON2CHA.DAX
MINOTAUR                -        MON1CHA.DAX
MINOTAUR                -        MON3CHA.DAX
MOBAT                   -        MON2CHA.DAX
MOBAT                   -        MON3CHA.DAX
MYRTANI                 -        MON3CHA.DAX
MYSELLIA                -        MON2CHA.DAX
OGRE                    -        MON1CHA.DAX
OGRE                    -        MON2CHA.DAX
OGRE                    -        MON3CHA.DAX
OGRE                    -        MON3CHA.DAX
PRISON LORD             01       MON2CHA.DAX
RED DRAGON              -        MON1CHA.DAX
RED DRAGON              -        MON3CHA.DAX
RED DRAGON              -        MON3CHA.DAX
SIR LEBAUM              01       MON3CHA.DAX
SIR RINGWALD            01       MON2CHA.DAX
SIVAK                   -        MON1CHA.DAX
SIVAK                   -        MON2CHA.DAX
SIVAK                   -        MON3CHA.DAX
SKELETAL DRAGON         -        MON2CHA.DAX
SKELETAL KNIGHT         01       MON2CHA.DAX
SKELETAL KNIGHT         01       MON3CHA.DAX
SKELETON                -        MON1CHA.DAX
SKELETON                -        MON3CHA.DAX
SKYLA                   -        MON2CHA.DAX
SKYLA                   -        MON3CHA.DAX
SOLDIER                 01       MON1CHA.DAX
SPECTRAL MINION         -        MON2CHA.DAX
STRANGBOURN             01       MON1CHA.DAX
THAUMATURGIST           -        MON2CHA.DAX
VARIANNA                01       MON2CHA.DAX
WARRIOR                 01       MON1CHA.DAX
WARRIOR                 01       MON3CHA.DAX
WHITE DRAGON            -        MON1CHA.DAX
ZOMBIE                  -        MON1CHA.DAX
ZOMBIE                  -        MON2CHA.DAX
ZOMBIE                  -        MON3CHA.DAX
